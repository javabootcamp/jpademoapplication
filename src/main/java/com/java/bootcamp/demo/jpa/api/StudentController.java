package com.java.bootcamp.demo.jpa.api;

import com.java.bootcamp.demo.jpa.orm.manager.StudentManager;
import com.java.bootcamp.demo.jpa.orm.model.Student;
import com.java.bootcamp.demo.jpa.orm.repository.StudentCRUDRepository;

import com.java.bootcamp.demo.jpa.orm.repository.StudentJPARepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

    private static Logger logger = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentCRUDRepository studentCRUDRepository;

    @Autowired
    private StudentJPARepository studentJPARepository;

    @Autowired
    private StudentManager studentManager;

    @RequestMapping
    public List<Student> allStudents(@RequestParam(required = false) String name){
        List<Student> students = new ArrayList<>();
        if(name == null) {
            studentCRUDRepository.findAll().forEach(students::add);
            // OR
            //return studentManager.allStudents();
        } else {
            studentJPARepository.findAllByName(name).forEach(students::add);
        }
        return students;
    }

    @RequestMapping(method = RequestMethod.POST)
    public Student saveStudent(@RequestBody  Student student){
        logger.info(student.toString());
        return studentCRUDRepository.save(student);
    }
}

package com.java.bootcamp.demo.jpa.orm.repository;

import com.java.bootcamp.demo.jpa.orm.model.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentCRUDRepository extends CrudRepository<Student,Long> {
}

package com.java.bootcamp.demo.jpa.orm.manager;

import com.java.bootcamp.demo.jpa.orm.model.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.List;

@Component
public class StudentManager {

    private static Logger logger = LoggerFactory.getLogger(StudentManager.class);

    @Autowired
    private EntityManager manager;

    public List<Student> allStudents(){
        return manager.createNativeQuery("SELECT * FROM STUDENT").getResultList();
    }

}

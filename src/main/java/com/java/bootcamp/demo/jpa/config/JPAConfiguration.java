package com.java.bootcamp.demo.jpa.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.java.bootcamp.demo.jpa.orm.repository")
public class JPAConfiguration {
}

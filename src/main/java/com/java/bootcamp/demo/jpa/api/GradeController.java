package com.java.bootcamp.demo.jpa.api;


import com.java.bootcamp.demo.jpa.orm.model.Grade;
import com.java.bootcamp.demo.jpa.orm.repository.GradeCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController("/grades")
public class GradeController {

    @Autowired
    private GradeCRUDRepository gradeCRUDRepository;
    
    @RequestMapping
    public List<Grade> allClasses(){
        List<Grade> grades = new ArrayList<>();
        gradeCRUDRepository.findAll().forEach(grades::add);
        return grades;
    }

}

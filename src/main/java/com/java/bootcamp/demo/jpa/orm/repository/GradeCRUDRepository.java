package com.java.bootcamp.demo.jpa.orm.repository;

import com.java.bootcamp.demo.jpa.orm.model.Grade;
import org.springframework.data.repository.CrudRepository;

public interface GradeCRUDRepository extends CrudRepository<Grade,Integer> {
}

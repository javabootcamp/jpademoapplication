package com.java.bootcamp.demo.jpa.orm.repository;

import com.java.bootcamp.demo.jpa.orm.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentJPARepository extends JpaRepository<Student,Long> {

    List<Student> findAllByName(String name);
}

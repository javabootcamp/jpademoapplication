package com.java.bootcamp.demo.jpa.orm.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Grade {

    @Id
    private Integer year;

    @Column
    private String room;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "year")
    private List<Student> students;


    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "year=" + year +
                ", room='" + room + '\'' +
                ", students=" + students +
                '}';
    }
}
